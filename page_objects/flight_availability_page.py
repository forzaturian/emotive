from page_objects.base_page import BasePage
from page_objects.locators import Locators


class FlightAvailabilityPage(BasePage):
    def flight_booking(self, num):
        self.click_elements(Locators.BOOK, num)
