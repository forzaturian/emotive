from selenium.webdriver.common.by import By


class Locators:
    SKIP_REGISTRATION = (By.CSS_SELECTOR, '._2KpZ6l._2doB4z')
    FLIGHTS_SECTION = (By.XPATH, "//a[contains(text(), 'Flights')]")
    LAX = (By.XPATH, "//div[@class ='_3uA4ax']//span[contains(text(), 'Los Angeles')]")
    LON = (By.XPATH, "//div[@class ='_3uA4ax']//span[contains(text(), 'London')]")
    DAY = (By.CSS_SELECTOR, '.jkj0H4._2v-mAi._3vGgRD')
    DEPARTURE = (By.CSS_SELECTOR, "._1w3ZZo._1YBGQV.lZd1T6._2vegSu._2mFmU7")
    ARRIVE = (By.CSS_SELECTOR, "._1w3ZZo._1YBGQV.lZd1T6._2dqBfU._2mFmU7")
    PASSENGERS = (By.CSS_SELECTOR, '._1w3ZZo.-Gn7o5._2mFmU7')
    PLUS_MINUS = (By.CSS_SELECTOR, '.VjWsXZ')
    SEARCH = (By.XPATH, "//span[contains(text(), 'SEARCH')]")
    BOOK = (By.CSS_SELECTOR, ".c-btn.u-link.secondary.enabled")
    LOGIN = (By.CSS_SELECTOR, "._3BiZc7._3wG3Kx")
