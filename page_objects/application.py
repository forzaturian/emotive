from page_objects.login_page import LoginPage
from page_objects.main_page import MainPage
from page_objects.travel_page import TravelPage
from page_objects.flight_availability_page import FlightAvailabilityPage


class Application:

    def __init__(self, driver):
        self.driver = driver

        self.login = LoginPage(self)
        self.main = MainPage(self)
        self.travel = TravelPage(self)
        self.fa = FlightAvailabilityPage(self)
