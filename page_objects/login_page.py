from page_objects.base_page import BasePage
from page_objects.locators import Locators


class LoginPage(BasePage):
    def page_validation(self):
        e = self.find_element(Locators.LOGIN)
        assert e.text == "LOGIN OR CREATE ACCOUNT"
