from page_objects.base_page import BasePage
from page_objects.locators import Locators


class MainPage(BasePage):
    def skipping_registration(self):
        self.single_click(Locators.SKIP_REGISTRATION)

    def navigate_to_flights(self):
        self.single_click(Locators.FLIGHTS_SECTION)

    def start_booking(self):
        self.skipping_registration()
        self.navigate_to_flights()