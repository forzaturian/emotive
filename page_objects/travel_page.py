from page_objects.base_page import BasePage
from page_objects.locators import Locators
from selenium.webdriver.support.ui import Select
from time import sleep


class TravelPage(BasePage):

    def searching_from(self, dep_city):
        self.input(Locators.DEPARTURE, dep_city)
        el = self.find_elements(Locators.LAX)
        el[0].click()

    def searching_to(self, arr_city):
        self.input(Locators.ARRIVE, arr_city)
        el = self.find_elements(Locators.LON)
        el[0].click()

    def select_date(self):
        self.single_click(Locators.DAY)

    def passengers_count(self, num1, num2, adults, infants):
        self.single_click(Locators.PASSENGERS)
        for i in range(0, adults - 1):
            self.click_elements(Locators.PLUS_MINUS, num1)

        for j in range(0, infants):
            self.click_elements(Locators.PLUS_MINUS, num2)

    def flight_search(self):
        self.single_click(Locators.SEARCH)

    def flight_criteria(self, dep_city, arr_city, num1, num2, adults, infants):
        self.searching_from(dep_city)
        self.searching_to(arr_city)
        self.select_date()
        self.passengers_count(num1, num2, adults, infants)
        self.flight_search()

