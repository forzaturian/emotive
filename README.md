# Emotive
Test case: Verify that not authenticated user will be redirected to the login page after booking flight

1.Go to https://www.flipkart.com/

2. Go to FLIGHTS menu and search flight to London

3. Use the next inputs:

“One Way”
From LA
To London
Departs - today
Return - in 3 days (can not be Return as it is a One Way flight)
Travelers - 2 Adults and 1 Infant
Class economy
4. Click the BOOK button

Set up:
1. to install requirements run the next command:
    pip install -r requirements.txt 

2. To set up virtual environment run the next commands:
    chmod u+x install.sh       
    ./install.sh
    source env/bin/activate

