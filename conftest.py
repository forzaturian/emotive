import pytest
from selenium import webdriver
from page_objects.application import Application


@pytest.fixture
def chrome_browser():
    options = webdriver.ChromeOptions()
    options.add_argument('start-fullscreen')
    options.add_argument('--ignore-certificate-errors')
    driver = webdriver.Chrome(options=options)
    yield driver
    driver.quit()


@pytest.fixture
def app(chrome_browser):
    page = Application(chrome_browser)
    page.main.go_to()
    return page
