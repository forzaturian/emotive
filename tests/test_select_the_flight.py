from time import sleep


def test_searching_for_flight(app):
    app.main.start_booking()
    app.travel.flight_criteria(dep_city="LAX", arr_city="LON", num1=1, num2=5, adults=2, infants=1)
    app.fa.flight_booking(2)
    app.login.page_validation()
